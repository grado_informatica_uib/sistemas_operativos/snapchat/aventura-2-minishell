/******************************************************************************
*                           MINISHELL - COMENTARIOS
*                       SISTEMAS OPERATIVOS: AVENTURA 2.
*                    GRADO EN INGENIERÍA INFORMÁTICA - UIB
*
*   AUTORES:     - JUAN CARLOS BOO CRUGEIRAS (carlosboocrugeiras@gmail.com)
*                - HÉCTOR MARIO MEDINA CABANELAS (hmedcab@gmail.com)
*   
*   FECHA:       10 DE DICIEMBRE DE 2018.
*
*   REPOSITORIO (PRIVADO):
* https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/aventura-2-minishell
*
 *****************************************************************************/

/******************************************************************************
* 1. INTRODUCCIÓN COMENTARIOS.
******************************************************************************/
ESTOS COMENTARIOS SE HAN REALIZADO COMO ANEXO A LA EXPLICACIÓN DETALLADA EN 
EL CÓDIGO FUENTE DEL PROGRAMA.
	- 1. INTRODUCCIÓN COMENTARIOS.
	- 2. FUNCIONES COMPLEMENTARIAS.
	- 3. DESARROLLOS AVANZAMOS.

/******************************************************************************
* 2. FUNCIONES COMPLEMENTARIAS.
******************************************************************************/

ESTAS FUNCIONES NO CONTEMPLAN NUEVAS FUNCIONALIDADES, SÓLO ENCAPSULAN CIERTAS 
CAPACIDADES PARA CLARIDAD DEL CÓDIGO Y PARA EVITAR SER REDUNDANTE.

- int check_cd(char **args);    

COMPRUEBA LA SINTXIS DE LOS COMANDOS PASADOS A INTERNAL_CD. DEVUELVE UN VALOR 
ENTERO 'BOOLEANO' EN FUNCIÓN DE SI ESTÁ BIEN O NO. 

- void concatenateTokens(char **args, char *nextDir); 

CONCATENA LOS TOKENS PARA GENERAR UNA ARRAY DE CHARS PARA PASÁRSELO COMO 
ARGUMENTO A LA INSTRUCCIÓN CHDIR EN EL INTERNAL_CD. 

ES NECESARIO HACERLO PARA DESARROLLAR EL INTERNAL_CD AVANZADO QUE PERMITE 
CAMBIAR A DIRECTORIOS CON ESPACIOS UTILIZANDO LA SIGUIENTE SINTAXIS:
		- "directorio con espacios"
		- 'directorio con espacios'
		- directorio\ con\ espacios

- void replaceChar(char *string, int replacedChar);  

SUSTITUYE CARACTERES '\' POR EL CARACTER ' ' EN LA LLAMADA A INTERNAL_CD, 
ES NECESARIO PARA IMPLEMENTAR EL INTERNAL_CD AVANZADO QUE PERMITA CAMBIOS 
DE DIRECTORIO CON LA SINTAXIS 
		- directorio\ con\ espacios

- void cleanString(char *string);     

ELIMINA CARACTERES " Y ' DEL STRING QUE SE LE PASARÁ AL INTERNAL_CD 
AVANZADO PARA QUE EJECUTE LOS DIRECTORIOS CON ESPACIOS:
		- "directorio con espacios"
		- 'directorio con espacios'


/******************************************************************************
* 2. DESAROOLOS AVANZADOS.
******************************************************************************/


-internal_cd()

SE HA AÑADIDO EL TRATAMIENTO DE DIRECTORIOS CON ESPACIOS CON LA SIGUIENTE 
SINTAXIS:
		- "directorio con espacios"
		- 'directorio con espacios'
		- directorio\ con\ espacios

- internal_source()
SE HA AÑADIDO TRATAMIENTO DE ERRORES CUANDO LA LINEA INTRODUCIDA CONTIENE 
MÁS DE DOS TOKENS.
		- source comandos.sh prueba    -----> DA ERROR DE SINTAXIS.


