/******************************************************************************
*                             MINISHELL - NIVEL 3 
*                       SISTEMAS OPERATIVOS: AVENTURA 2.
*                    GRADO EN INGENIERÍA INFORMÁTICA - UIB
*
*   AUTORES:     - JUAN CARLOS BOO CRUGEIRAS (carlosboocrugeiras@gmail.com)
*                - HÉCTOR MARIO MEDINA CABANELAS (hmedcab@gmail.com)
*   
*   FECHA:       06 DE DICIEMBRE DE 2018.
*
*   REPOSITORIO (PRIVADO):
* https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/aventura-2-minishell
*
 *****************************************************************************/

/******************************************************************************
 *                       DEFINICIONES DE VARIABLES  
 *****************************************************************************/
#define _POSIX_C_SOURCE         200112L     // 
/* CONSTANTES DE LINEA */
#define COMMAND_LINE_SIZE       1024        // Longitud máxima de la linea.
#define ARGS_SIZE               64          // Número máximo de argumentos.
/*  CONSTANTES DE VISUALIZACIÓN. */
#define PROMPT                  "$"         // Caracter prompt
#define SEPARATOR               "-->"       // Separador.
#define COLOR_BLUE              "\e[34;1m"  // Color azul.
#define COLOR_WHITE             "\e[37;0m"  // Color blanco.
#define COLOR_YELLOW            "\e[33;1m"  // Color amarillo.
#define COLOR_RED               "\e[31;1m"  // Color rojo.
/* CONSTANTES TOKENS KEYS PERMITIDOS */
#define TOKEN_KEYS               " \t\n\r"  // Caracteres para tokenizar.
#define COMMENT_WILDCARD         35         // Entero de ASCII '#'

/******************************************************************************
 *                          LIBRERÍAS NECESARIAS 
 *****************************************************************************/
#include<stdio.h>
#include <stdlib.h>
#include<string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/wait.h>

/******************************************************************************
 *                          FUNCIONES UTILIZADAS  
 *****************************************************************************/
void imprimir_prompt();                     // Imprime el prompt.
char *read_line(char *line);                // Lee una línea de stdin
int execute_line(char *line);               // Ejecuta una línea
int parse_args(char **args, char *line);    // Tokeniza la línea
int check_internal(char **args);            // Comprueba si es comando interno
int internal_cd(char **args);               // Cambio de directorio
int check_cd(char **args);                  // Comprueba sintaxis cd
void concatenateTokens(char **args, char *nextDir); // Concatena tokens
void replaceChar(char *string, int replacedChar);  // Sustituye un caracter
void cleanString(char *string);             // Elimina " y ' de un string
int internal_export(char **args);           // Modifica variables entorno
int internal_source(char **args);           // Ejecuta fichero
int internal_jobs(char **args);             // Muestra PID procesos background

/******************************************************************************
 *                                   MAIN
 *****************************************************************************/
void main(){
    // Reservamos espacio de memoria para linea.
    char *line = malloc(COMMAND_LINE_SIZE);
    // Si hay algún error en malloc()
    if(line == NULL){
        fprintf(stderr, "Error: %s \n", strerror(errno));
    }
    // Mientras haya comandos que leer
    while(read_line(line)){
        // Ejecutamos comandos
        execute_line(line);
    }
    // Liberamos espacio de line
    free(line);
}

/******************************************************************************
*   NOMBRE:       IMPRIMIR_PROMPT();
*   ARGUMENTOS:   NINGUNO.
*   DEVUELVE:     NADA.
*   DESCRIPCIÓN:  LA FUNCIÓN IMPRIME UN ARRAY DE CHAR:
*                   - nombre_usuario-->directorio_actual$
 *****************************************************************************/
void imprimir_prompt(){
    // Reservamos espacio en memoria para el prompt.
    char *prompt = malloc(1000);
    // Si hay algún error en malloc()
    if(prompt == NULL){
        fprintf(stderr, "Error: %s \n", strerror(errno));
    }
    // Añadimos color azul.
    strcpy(prompt, COLOR_BLUE);
    // Añadimos el nombre de usuario.
    strcat(prompt, getenv("USER"));
    // Añadimos color amarillo.
    strcat(prompt, COLOR_YELLOW);
    // Añadimos String separador.
    strcat(prompt, SEPARATOR);
    // Añadimos color rojo.
    strcat(prompt, COLOR_RED);
    // Añadimos directorio actual.
    strcat(prompt, getenv("PWD"));
    // Añadimos el caracter prompt.
    strcat(prompt, PROMPT);
    // Añadimos el color blanco.
    strcat(prompt, COLOR_WHITE);
    // Añadimos un espacio en blanco.
    strcat(prompt, " ");
    // imprimimos el prompt.
    printf("%s", prompt);
    // Liberamos espacio de memoria.
    free(prompt);
}

/******************************************************************************
*   NOMBRE:       READ_LINE();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
char *read_line(char *line){  
    // Imprimimos el prompt.  
    imprimir_prompt();
    // Limpiamos el stream stdout para asegurar que no queda nada allí.
    fflush(stdout);
    // Leemos la línea que ha introducido el usuario.
    line = fgets(line, COMMAND_LINE_SIZE, stdin);
    // Devolvemos line.
    return line;
}

/******************************************************************************
*   NOMBRE:       EXECUTE_LINE();
*   ARGUMENTOS:
*   DEVUELVE:     0 - Todo ha ido bien.
*                -1 - Ha habido algún error.
*   DESCRIPCIÓN:
 *****************************************************************************/
int execute_line(char *line){
    // Creamos doble puntero
    char **args = malloc(ARGS_SIZE * sizeof(char *));
    // Si args es NULL ha habido un problema con malloc()
    if(args == NULL){
        fprintf(stderr, "Error: %s \n", strerror(errno));
        return -1;
    }
    // Guardamos en numTokens el número de tokens de line
    int numTokens = parse_args(args, line);
    // Comprobamos si se trata de un comando interno y si lo es se ejecuta.
    int internalCommand = check_internal(args);
    // Si se trata de un comando externo
    if(internalCommand == 0){
        // Creamos un proceso hijo.
        int childPID = fork();
        // Si el que está en ejecución es el hijo.
        if(childPID == 0){
            // Imprimimos el ID del padre.
            printf("[execute_line()→ PID padre: %d]\n", getppid());
            // Imprimimos el ID del hijo.
            printf("[execute_line()→ PID hijo: %d]\n", getpid());
            // Ejecutamos el comando con los argumentos.
            if(execvp(args[0], args) == -1){
                // Si el comando no existe.
                fprintf(stderr, "%s: no se encontró la orden \n", *args);
                exit(EXIT_FAILURE);
            }
            exit(EXIT_SUCCESS);
        } // Si el que está ejecución es el proceso hijo.
        else {
            // Declaramos estado
            int status;
            // Esperamos hasta que el proceso hijo acaba y guardamos su estado en status
            wait(&status);
            // Si se ha salido con 'exit'
            if(WIFEXITED(status)){
                // Imprimimos información pertinente.
                printf("[execute_line()→ Proceso hijo %d finalizado con exit(), estado: %d]\n",childPID,WEXITSTATUS(status));       
            }
        }

    }
    // No se trata de un comando externo
    else {

    }
    // Liberamos memoria del puntero.
    free(args);
    // Devolvemos 0.
    return 0;
}

/******************************************************************************
*   NOMBRE:       PARSE_ARGS();
*   ARGUMENTOS:
*   DEVUELVE:      
*   DESCRIPCIÓN:
 *****************************************************************************/
int parse_args(char **args, char *line){
    // Contador de tokens.
    int tokenCounter = 0;
    // Cogemos el primer token y lo guardamos.
    *args = strtok(line, TOKEN_KEYS);
    // Informamos por consola de lo ocurrido.
    //printf("[parse_args()→ token %d: %s]\n",tokenCounter, *args);
    // Mientras haya tokens.
    while(*args != NULL){
        // Si el Token es un comentario.
        if(*args[0] == COMMENT_WILDCARD){
            // Nos aseguramos lo que viene detrás no será tratado.
            *args = NULL;
            // Imprimimos mensaje de información.
            //printf("[parse_args()-> token %d corregido: %s]\n", tokenCounter, *args);
        }
        // Si el token no es un comentario.
        else {
            // Avanzamos en el array.
            args++;
            // Actualizamos el contador de tokens.
            tokenCounter++;
            // Recogemos el siguiente token.
            *args = strtok(NULL, TOKEN_KEYS);
            // Imprimimos en pantalla lo ocurrido.
            //printf("[parse_args()→ token %d: %s]\n",tokenCounter, *args);
        }
    }
    return tokenCounter;
}

/******************************************************************************
*   NOMBRE:       ChECK_INTERNAL();
*   ARGUMENTOS:
*   DEVUELVE:     1 - Comando interno
*                 0 - Comando externo
*                -1 - Comando nulo (NULL)
*   DESCRIPCIÓN:
 *****************************************************************************/
int check_internal(char **args){
    // Si no hay ningún token.
    if(*args == NULL) return -1;
    // Si se ha introducido "exit".
    if(strcmp("exit", *args) == 0)      exit(0);
    // Si se ha introducido "cd"
    if(strcmp("cd", *args) == 0)        return internal_cd(args);
    // Si se ha introducido "export"
    if(strcmp("export", *args) == 0)    return internal_export(args);
    // Si se ha introducido "source"
    if(strcmp("source", *args) == 0)    return internal_source(args);
    // Si se ha introducido "jobs"
    if(strcmp("jobs", *args) == 0)      return internal_jobs(args);
    // Si hemos llegado hasta aquí significa que es un comando externo.
    return 0;
}

/******************************************************************************
*   NOMBRE:       INTERNAL_CD();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
int internal_cd(char **args){
    // Directorio Actual
    char actualDir[COMMAND_LINE_SIZE];
    // Avanzamos al siguiente token
    args++;
    // Declaramos un puntero tipo char para alojar el directorio al que iremos.
    char *nextDir = malloc(COMMAND_LINE_SIZE);
    // Si ha habido algun error en el malloc()
    if(nextDir == NULL){
        fprintf(stderr, "Error: %s \n", strerror(errno));
        return -1;
    }
    // Si no hay argumento
    if(*args == NULL){
        strcpy(nextDir, "/home/");
        strcat(nextDir, getenv("USER"));
    } else { // Si hay argumento
        // Comprobammos si la sintaxis es correcta
        if(check_cd(args)!= 1){
            // Imprimimos información del error
            fprintf(stderr, "Error de sintaxis. Demasiados argumentos\n");
            // Devolvemos -1 para saber que es un error.
            return -1;
        }
        // Concatenamos los tokens
        concatenateTokens(args, nextDir);
        // Reemplazamos los caracteres '\' si los hubiera
        replaceChar(nextDir, 92);
        // Limpiamos caracteres "  y ' si los hubiera
        cleanString(nextDir);
    }
    // Si se produce un error al cambiar de directorio
    if(chdir(nextDir) != -1){
        // Cambiamos el valor de la variable de entorno PWD
        if(getcwd(actualDir, sizeof(actualDir)) == NULL){
            fprintf(stderr, "%s\n", strerror(errno));
        }
        else{
            setenv("PWD", actualDir,1);
        }
        // Imprimimos la información.
        //printf("[internal_cd()-> %s]\n", actualDir);
    } else { // Si se ha producido un error
        // Imprimimos el error.
        fprintf(stderr, "chdir: %s \n", strerror(errno));
        // Devolvemos -1 porque ha habido un error.
        return -1;
    }
    free(nextDir);
    return 1;
}

/******************************************************************************
*   NOMBRE:       CHECK_CD();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
int check_cd(char **args){
    // Si hay directorio con espacios, el último elemento del token0 será "\"
    if(args[0][strlen(args[0])-1] == 92){
        // Recorremos los tokens
        for(int i = 1; args[i] != NULL; i++){
            // Si encontramos un token que no tenga "\" final es que hemos acabado
            if(args[i][strlen(args[i])-1] != 92){
                // Si el siguiente token no es null hay un error de sintaxis.
                if(args[i+1] != NULL) return -1;
            }
        }
    }
    // Si hay directorio con espacios, el token0 tendrá el caracter "
    if(strchr(args[0], 34) != NULL){
        for(int i = 1; args[i] != NULL; i++){
            if(strchr(args[i], 34) != NULL){
                // Si el siguiente token no es null hay un error de sintaxis.
                if(args[i+1] != NULL) return -1;
            }
        }
    }
   // Si hay directorio con espacios, el token0 tendrá el caracter '
    if(strchr(args[0], 39) != NULL){
        for(int i = 1; args[i] != NULL; i++){
            if(strchr(args[i], 39) != NULL){
                // Si el siguiente token no es null hay un error de sintaxis.
                if(args[i+1] != NULL) return -1;
            }
        }
    }
    return 1;
}
/******************************************************************************
*   NOMBRE:       CONCATENATETOKENS();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
void concatenateTokens(char **args, char *nextDir){
    // Variable 'booleana' que controla si se encuentr o no un caracter especial
    int boolean = 0;
    // Mientras haya tokens
    for(int i=0;args[i] != NULL;i++){
        // Concatenamos el token
        strcat(nextDir, args[i]);
        // Si encontramos un caracter ' o "
        if(strchr(args[i], 39) || strchr(args[i], 34)){
            // Si habiamos encontrado uno
            if(boolean == 1){
                // Ahora encontramos la pareja.
                boolean = 0;
            } else{ // Si no haíamos encontrado ninguno
                // Ahora hemos encontrado el primero
                boolean = 1;
            }
        }
        // Si hay un caracter especial 
        if(boolean == 1){
            // Concatenamos un espacio porque "Directorio con espacios"
            strcat(nextDir, " ");
        }
    }

}

/******************************************************************************
*   NOMBRE:       REPLACECHAR();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
void replaceChar(char *string, int replacedChar){
    // Mientras no lleguemos al final de string
    for(int i = 0;string[i] != '\0'; i++){
        // Si el caracter es igual al caracter que queremos reemplazar
        if(string[i] == replacedChar){
            // Reemplazamos ese elemento por un espacio
            string[i] = ' ';
        }
    }
}

/******************************************************************************
*   NOMBRE:       CLEANSTRING();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
void cleanString(char *string){
    // Variables índices de control
    int j = 0;
    int k = 0;
    // Recorremos el string
    for(int i = 0;string[i] != '\0'; i++){
        // Si encontramos un caracter ' o ""
        if(string[i] == 34 || string[i] == 39){
            j = i;
            k = i + 1;
            // Recorremos el resto para actualizar el array
            while(string[k] != '\0'){
                string[j] = string[k];
                j++;
                k++;
            }
            // Final de string
            string[j] = '\0';
        }
   }
}



/******************************************************************************
*   NOMBRE:       INTERNAL_EXPORT();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
int internal_export(char **args){
    args++;
    // Si se introduce sólo 'export' salta este error.
    if(*args == NULL){
        fprintf(stderr,"Error de sintaxis. Uso: export Nombre=Valor \n");
        return -1;
    }
    // Comprobamos que solo hay 2 tokens, si no, error
    args++;
    // Si hay un tercer token, error
    if(*args != NULL){
        fprintf(stderr,"Error de sintaxis. Uso: export Nombre=Valor \n");
        return -1;
    }
    // Volvemos a colocar la instrucción en el puntero
    args--;
    // Creamos dos punteros para trocear la instrucción
    char *nombre = malloc(sizeof(COMMAND_LINE_SIZE)); // antiguo nombre de la variable
    char *valor = malloc(sizeof(COMMAND_LINE_SIZE));  // nuevo nombre de la variable
    // Guardamos lo anterior al "=" en 'nombre'
    int i;
    for(i = 0; args[0][i] != '=' && args[0][i] != '\0'; i++){
        nombre[i] = args[0][i];
    }
    nombre[i] = '\0';
    // Si no hay "=" en el medio, es un error de sintaxis
    if(args[0][i] != '='){
        //printf("[internal_export()→ nombre: %s]\n", nombre);
        valor = NULL;
        //printf("[internal_export()→ valor: %s]\n", valor);
        fprintf(stderr,"Error de sintaxis. Uso: export Nombre=Valor \n");
        return -1;
    }
    i++;
    // Guardamos el string posterior al "=" en 'valor'
    int a;
    for(a = 0; args[0][i] != '\0'; a++){
        valor[a] = args[0][i];
        i++;
    }
    valor[a] = '\0';
    // Imprimimos por pantalla los cambios realizados
    //printf("[internal_export() -> nombre: %s]\n", nombre);
    //printf("[internal_export() -> valor: %s]\n", valor);
    //printf("[internal_export() -> antiguo valor para %s: %s]\n", nombre, getenv(nombre));
    // Cambiamos el valor de la variable
    setenv(nombre, valor, 1);
    //printf("[internal_export() -> nuevo valor para %s: %s]\n", nombre, getenv(nombre));
    free(nombre);
    free(valor);
    return 1;
}

/******************************************************************************
*   NOMBRE:       INTERNAL_SOURCE();
*   ARGUMENTOS:   Nombre del fichero
*   DEVUELVE:     1
*   DESCRIPCIÓN:  Ejecuta los comandos de las lineas del fichero indicado.
 *****************************************************************************/
int internal_source(char **args){
    // Comprobamos primero que la sintaxis es correcta
    if(args[2] == NULL){
        if(args[1] != NULL){
            // Abrimos el fichero
            FILE * file = fopen(args[1], "r");
            // Creamos una array donde se alojarán los datos
            char linea[COMMAND_LINE_SIZE];
            if(file){
                // Comenzamos la lectura del fichero
                char *pointer = fgets(linea, COMMAND_LINE_SIZE, (FILE*) file);
                // Después de cada lectura, realizamos un fflush
                fflush(file);
                while(pointer != NULL){
                    printf("[internal_source()→ LINE: %s ]\n", pointer);
                    // Ejecutamos las lineas que leemos del fichero
                    execute_line(pointer);
                    pointer = fgets(linea, COMMAND_LINE_SIZE, (FILE*) file);
                    fflush(file);
                }
                fclose(file);
            } else {
                // Si no existe el fichero, error
                fprintf(stderr, "fopen: %s\n", strerror(errno));
            }
        } else{
            // Si no se escribe el nombre de fichero como parámetro, error
            fprintf(stderr, "Error de sintaxis. Uso: source <nombre_fichero>\n");
        }
    } else {
        // Si hay más de dos tokens, error
        fprintf(stderr, "Error de sintaxis. Uso: source <nombre_fichero>\n");
    }
    return 1;  
}

/******************************************************************************
*   NOMBRE:       INTERNAL_JOBS();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
int internal_jobs(char **args){
    //printf("[internal_jobs()-> Esta función mostrará el PID de los procesos que no estén en foreground]\n");
    return 1;
}