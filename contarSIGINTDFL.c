#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

void main(int count_limit, char *argv[]){
	signal(SIGINT, SIG_DFL);
	int numero = atoi(argv[1]);
	for(int count = 0; count < numero; count++){
		printf("%d\n", count);
		sleep(1);
	}
}
