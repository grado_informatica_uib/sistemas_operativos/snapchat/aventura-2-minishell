/******************************************************************************
*                             MINISHELL - NIVEL 2 
*                       SISTEMAS OPERATIVOS: AVENTURA 2.
*                    GRADO EN INGENIERÍA INFORMÁTICA - UIB
*
*   AUTORES:     - JUAN CARLOS BOO CRUGEIRAS (carlosboocrugeiras@gmail.com)
*                - HÉCTOR MARIO MEDINA CABANELAS (hmedcab@gmail.com)
*   
*   FECHA:       05 DE DICIEMBRE DE 2018.
*
* https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/aventura-2-minishell
*
 *****************************************************************************/

/******************************************************************************
 *                       DEFINICIONES DE VARIABLES  
 *****************************************************************************/
#define _POSIX_C_SOURCE         200112L     // 
/* CONSTANTES DE LINEA */
#define COMMAND_LINE_SIZE       1024        // Longitud máxima de la linea.
#define ARGS_SIZE               64          // Número máximo de argumentos.
/*  CONSTANTES DE VISUALIZACIÓN. */
#define PROMPT                  "$"         // Caracter prompt
#define SEPARATOR               "-->"       // Separador.
#define COLOR_BLUE              "\e[34;1m"  // Color azul.
#define COLOR_WHITE             "\e[37;0m"  // Color blanco.
#define COLOR_YELLOW            "\e[33;1m"  // Color amarillo.
#define COLOR_RED               "\e[31;1m"  // Color rojo.
/* CONSTANTES TOKENS KEYS PERMITIDOS */
#define TOKEN_KEYS               " \t\n\r"  // Caracteres para tokenizar.
#define COMMENT_WILDCARD         35         // Entero de ASCII '#'

/******************************************************************************
 *                          LIBRERÍAS NECESARIAS 
 *****************************************************************************/
#include<stdio.h>
#include <stdlib.h>
#include<string.h>
#include <errno.h>

/******************************************************************************
 *                          FUNCIONES UTILIZADAS  
 *****************************************************************************/
void imprimir_prompt();                     // Imprime el prompt.
char *read_line(char *line);                // Lee una línea de stdin
int execute_line(char *line);               // Ejecuta una línea
int parse_args(char **args, char *line);    // Tokeniza la línea
int check_internal(char **args);            // Comprueba si es comando interno
int internal_cd(char **args);               // Cambio de directorio
int internal_export(char **args);           // Modifica variables entorno
int internal_source(char **args);           // Ejecuta fichero
int internal_jobs(char **args);             // Muestra PID procesos background

/******************************************************************************
 *                                   MAIN
 *****************************************************************************/
void main(){
    // Reservamos espacio de memoria para linea.
    char *line = malloc(COMMAND_LINE_SIZE);
    // Si hay algún error en malloc()
    if(errno){
        fprintf(stderr, "Error: %s \n", strerror(errno));
    }
    // Mientras haya comandos que leer
    while(read_line(line)){
        // Ejecutamos comandos
        execute_line(line);
    }
    // Liberamos espacio de line
    free(line);
    // Si hay algún error en free()
    if(errno){
        fprintf(stderr, "Error: %s \n", strerror(errno));
    }
}

/******************************************************************************
*   NOMBRE:       IMPRIMIR_PROMPT();
*   ARGUMENTOS:   NINGUNO.
*   DEVUELVE:     NADA.
*   DESCRIPCIÓN:  LA FUNCIÓN IMPRIME UN ARRAY DE CHAR:
*                   - nombre_usuario-->directorio_actual$
 *****************************************************************************/
void imprimir_prompt(){
    // Reservamos espacio en memoria para el prompt.
    char *prompt = malloc(1000);
    // Si hay algún error en malloc()
    if(errno){
        fprintf(stderr, "Error: %s \n", strerror(errno));
    }
    // Añadimos color azul.
    strcpy(prompt, COLOR_BLUE);
    // Añadimos el nombre de usuario.
    strcat(prompt, getenv("USER"));
    // Añadimos color amarillo.
    strcat(prompt, COLOR_YELLOW);
    // Añadimos String separador.
    strcat(prompt, SEPARATOR);
    // Añadimos color rojo.
    strcat(prompt, COLOR_RED);
    // Añadimos directorio actual.
    strcat(prompt, getenv("PWD"));
    // Añadimos el caracter prompt.
    strcat(prompt, PROMPT);
    // Añadimos el color blanco.
    strcat(prompt, COLOR_WHITE);
    // Añadimos un espacio en blanco.
    strcat(prompt, " ");
    // imprimimos el prompt.
    printf("%s", prompt);
    // Liberamos espacio de memoria.
    free(prompt);
    // Si hay algún error en free()
    if(errno){
        fprintf(stderr, "Error: %s \n", strerror(errno));
    }
}

/******************************************************************************
*   NOMBRE:       READ_LINE();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
char *read_line(char *line){  
    // Imprimimos el prompt.  
    imprimir_prompt();
    // Limpiamos el stream stdout para asegurar que no queda nada allí.
    fflush(stdout);
    // Leemos la línea que ha introducido el usuario.
    line = fgets(line, COMMAND_LINE_SIZE, stdin);
    // Devolvemos line.
    return line;
}

/******************************************************************************
*   NOMBRE:       EXECUTE_LINE();
*   ARGUMENTOS:
*   DEVUELVE:     0 - Todo ha ido bien.
*                -1 - Ha habido algún error.
*   DESCRIPCIÓN:
 *****************************************************************************/
int execute_line(char *line){
    // Creamos doble puntero
    char **args = malloc(ARGS_SIZE * sizeof(char *));
    // Si args es NULL ha habido un problema con malloc()
    if(errno){
        fprintf(stderr, "Error: %s \n", strerror(errno));
        return -1;
    }
    // Guardamos en numTokens el número de tokens de line
    int numTokens = parse_args(args, line);
    // Comprobamos si se trata de un comando interno y si lo es se ejecuta.
    check_internal(args);
    // Liberamos memoria del puntero.
    free(args);
    if(errno){
        fprintf(stderr, "Error: %s \n", strerror(errno));
        return -1;
    }
    // Devolvemos 0.
    return 0;
}

/******************************************************************************
*   NOMBRE:       PARSE_ARGS();
*   ARGUMENTOS:
*   DEVUELVE:      
*   DESCRIPCIÓN:
 *****************************************************************************/
int parse_args(char **args, char *line){
    // Contador de tokens.
    int tokenCounter = 0;
    // Cogemos el primer token y lo guardamos.
    *args = strtok(line, TOKEN_KEYS);
    // Informamos por consola de lo ocurrido.
    printf("[parse_args()→ token %d: %s]\n",tokenCounter, *args);
    // Mientras haya tokens.
    while(*args != NULL){
        // Si el Token es un comentario.
        if(*args[0] == COMMENT_WILDCARD){
            // Nos aseguramos lo que viene detrás no será tratado.
            *args = NULL;
            // Imprimimos mensaje de información.
            printf("[parse_args()-> token %d corregido: %s]\n", tokenCounter, *args);
        }
        // Si el token no es un comentario.
        else {
            // Avanzamos en el array.
            args++;
            // Actualizamos el contador de tokens.
            tokenCounter++;
            // Recogemos el siguiente token.
            *args = strtok(NULL, TOKEN_KEYS);
            // Imprimimos en pantalla lo ocurrido.
            printf("[parse_args()→ token %d: %s]\n",tokenCounter, *args);
        }
    }
    return tokenCounter;
}

/******************************************************************************
*   NOMBRE:       CKECK_INTERNAL();
*   ARGUMENTOS:
*   DEVUELVE:     1 - Comando interno
*                 0 - Comando externo
*                -1 - Comando nulo (NULL)
*   DESCRIPCIÓN:
 *****************************************************************************/
int check_internal(char **args){
    // Si no hay ningún token.
    if(*args == NULL) return -1;
    // Si se ha introducido "exit".
    if(strcmp("exit", *args) == 0)      exit(0);
    // Si se ha introducido "cd"
    if(strcmp("cd", *args) == 0)        return internal_cd(args);
    // Si se ha introducido "export"
    if(strcmp("export", *args) == 0)    return internal_export(args);
    // Si se ha introducido "source"
    if(strcmp("source", *args) == 0)    return internal_source(args);
    // Si se ha introducido "jobs"
    if(strcmp("jobs", *args) == 0)      return internal_jobs(args);
    // Si hemos llegado hasta aquí significa que es un comando externo.
    return 0;
}

/******************************************************************************
*   NOMBRE:       INTERNAL_CD();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
int internal_cd(char **args){
    printf("[internal_cd()-> Esta función cambiará de directorio]\n");
    return 1;
}

/******************************************************************************
*   NOMBRE:       INTERNAL_EXPORT();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
int internal_export(char **args){
    printf("[internal_export()-> Esta función asignará valores a variables de entorno]\n");
    return 1;
}

/******************************************************************************
*   NOMBRE:       INTERNAL_SOURCE();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
int internal_source(char **args){
    printf("[internal_source()-> Esta función ejecutará un fichero de líneas de comandos]\n");
    return 1;
}

/******************************************************************************
*   NOMBRE:       INTERNAL_JOBS();
*   ARGUMENTOS:
*   DEVUELVE:     
*   DESCRIPCIÓN:
 *****************************************************************************/
int internal_jobs(char **args){
    printf("[internal_jobs()-> Esta función mostrará el PID de los procesos que no estén en foreground]\n");
    return 1;
}