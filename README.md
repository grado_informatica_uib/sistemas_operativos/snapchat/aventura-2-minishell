# **ADVENTURE 2:  MINISHELL**

## 1. **INTRODUCTION**

This project pretends to emulate the unix bash's behavior. Its completely developed using C language.

## 2.**DEVELOPERS:** 
* Juan Carlos Boo Crugeiras (carlosboocrugeiras@gmail.com)
* Héctor Mario Medina Cabanelas  (hmedcab@gmail.com)

## 3. **CONTENT:**

The content is organized into 7 level developments, which are the following ones:

### **LEVEL 1**

In level one we are able to read command lines continously. That command will be cut in tokens. If the command is an internal one, we will print its task in the screen.
* `cd()`
* `export()`
* `source()`
* `jobs()`

More information [Level 1 documentation](https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/minishell/blob/master/documentation/nivel1.md)

### **LEVEL 2**

More information [Level 2 documentation](https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/minishell/blob/master/documentation/nivel2.md)

### **LEVEL 3**

More information [Level 3 documentation](https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/minishell/blob/master/documentation/nivel3.md)

### **LEVEL 4**

More information [Level 4 documentation](https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/minishell/blob/master/documentation/nivel4.md)

### **LEVEL 5**

More information [Level 5 documentation](https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/minishell/blob/master/documentation/nivel5.md)

### **LEVEL 6**

More information [Level 6 documentation](https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/minishell/blob/master/documentation/nivel6.md)

### **LEVEL 7**

More information [Level 7 documentation](https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/minishell/blob/master/documentation/nivel7.md)
