/******************************************************************************
*                             MINISHELL - NIVEL 5 
*                       SISTEMAS OPERATIVOS: AVENTURA 2.
*                    GRADO EN INGENIERÍA INFORMÁTICA - UIB
*
*   AUTORES:     - JUAN CARLOS BOO CRUGEIRAS (carlosboocrugeiras@gmail.com)
*                - HÉCTOR MARIO MEDINA CABANELAS (hmedcab@gmail.com)
*   
*   FECHA:       10 DE DICIEMBRE DE 2018.
*
*   REPOSITORIO (PRIVADO):
* https://gitlab.com/grado_informatica_uib/sistemas_operativos/snapchat/aventura-2-minishell
*
 *****************************************************************************/
/******************************************************************************
 *                          LIBRERÍAS NECESARIAS 
 *****************************************************************************/
#define _POSIX_C_SOURCE         200112L     // Estandar.
#include<stdio.h>
#include <stdlib.h>
#include<string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include <signal.h>

/******************************************************************************
 *                       DEFINICIONES DE VARIABLES  
 *****************************************************************************/
/* CONSTANTES DE LINEA */
#define COMMAND_LINE_SIZE       1024        // Longitud mÃ¡xima de la linea.
#define ARGS_SIZE               64          // NÃºmero mÃ¡ximo de argumentos.
/*  CONSTANTES DE VISUALIZACIÃN. */
#define PROMPT                  "$"         // Caracter prompt
#define SEPARATOR               "-->"       // Separador.
#define COLOR_BLUE              "\e[34;1m"  // Color azul.
#define COLOR_WHITE             "\e[37;0m"  // Color blanco.
#define COLOR_YELLOW            "\e[33;1m"  // Color amarillo.
#define COLOR_RED               "\e[31;1m"  // Color rojo.
/* CONSTANTES TOKENS KEYS PERMITIDOS */
#define TOKEN_KEYS               " \t\n\r"  // Caracteres para tokenizar.
#define COMMENT_WILDCARD         35         // Entero de ASCII '#'

static char SHELL_COMMAND_LINE[] = "./nivel5"; // Comando para ejercutar shell.
/* CONSTANTES Y ESTRUCTURA DE PILA DE PROCESOS */
#define N_JOBS                   64         // Número máximo de procesos.
struct info_process {                       // Estrucura de la pila.
    pid_t pid;                              // Identificador de proceso.
    char status;                            // Estado: E, D, F
    char command_line[COMMAND_LINE_SIZE];   // Comando del proceso.
};
static struct info_process jobs_list[N_JOBS];// Declaración de la pila.
int n_pids = 0;                              // Número de procesos en pila.

/******************************************************************************
 *                          FUNCIONES UTILIZADAS  
 *****************************************************************************/
void imprimir_prompt();                     // Imprime el prompt.
char *read_line(char *line);                // Lee una línea de stdin
int execute_line(char *line);               // Ejecuta una línea
int is_background(char *line);              // Comprueba comando background.
int jobs_list_add(pid_t pid, char status, char * command_line); // Add proceso
int jobs_list_find(pid_t pid);              // Encontra un proceso.
int jobs_list_remove(int pos);              // Elimina un proceso.
int parse_args(char **args, char *line);    // Tokeniza la línea
int check_internal(char **args);            // Comprueba si es comando interno
int internal_cd(char **args);               // Cambio de directorio
int check_cd(char **args);                  // Comprueba sintaxis cd
void concatenateTokens(char **args, char *nextDir); // Concatena tokens
void replaceChar(char *string, int replacedChar);  // Sustituye un caracter
void cleanString(char *string);             // Elimina " y ' de un string
int internal_export(char **args);           // Modifica variables entorno
int internal_source(char **args);           // Ejecuta fichero
int internal_jobs(char **args);             // Muestra PID procesos background
void ctrlz(int signum);                     // Manejador de la señal SIGTSTP
void ctrlc(int signum);                     // Manejador de la señal SIGINT
void reaper(int signum);                    // Manejador de la señal SIGCHLD


/******************************************************************************
 *                                   MAIN
 *****************************************************************************/
void main(){
    // Asociamos la señal SIGTSTP con la función Control+Z
    signal(SIGTSTP, ctrlz);
    // Asociamos al enterrador de zombies cuando un hijo acaba (señal SIGCHLD)
    signal(SIGCHLD, reaper); 
    // Asociamos a la función ctrlc() cuando se pulsa Ctrl+C
    signal(SIGINT, ctrlc); 
    // Reservamos espacio de memoria para linea.
    char *line = malloc(COMMAND_LINE_SIZE);
    // Inicializamos jobs_list[0]
    jobs_list[0].pid = 0;
    jobs_list[0].status = 'F';
    strcpy(jobs_list[0].command_line, " ");
    // Si hay algÃºn error en malloc()
    if(line == NULL){
        fprintf(stderr, "Error: %s \n", strerror(errno));
    }
    // Mientras haya comandos que leer
    while(read_line(line)){
        // Ejecutamos comandos
        execute_line(line);
        // Vaciamos la linea de comandos
        line[0] = '\0';
    }
    // Liberamos espacio de line
    free(line);
}

/******************************************************************************
*   NOMBRE:        REAPER();
*   ARGUMENTOS:    INT SIGNUM
*   DEVUELVE:      VOID
*   DESCRIPCIÓN:   SE ENCARGA DE GESTIONAR LOS PROCESOS FINALIZADOS Y 
*                  ACTUALIZAR LA PILA DE PROCESOS SI LO HACEN.
 *****************************************************************************/
void reaper(int signum){
    // Asociamos a la señal SIGCHLD la función reaper
    signal(SIGCHLD, reaper);
    int status;
    pid_t pid = waitpid(-1, &status, WNOHANG);
    if(pid == 0){
        pid = jobs_list[0].pid;
    }
    int i = jobs_list_find(pid);
    if(i != 0){  
        if(WTERMSIG(status)){
            printf("\n[reaper()→ Proceso hijo %d en background (%s) finalizado por señal %d]     \n", jobs_list[i].pid, jobs_list[i].command_line, WTERMSIG(status));
        } else {
            // Informamos de lo ocurrido
            printf("\n[reaper()→ Proceso hijo %d en background (%s) finalizado con exit code %d]       \n", jobs_list[i].pid, jobs_list[i].command_line, WEXITSTATUS(status));
        }
        printf("Terminado PID %d (%s) en jobs_list[%d] con status %d \n",pid, jobs_list[i].command_line, i, status);
    } else {
       if(WEXITSTATUS(status) != 127){
            if(WTERMSIG(status)){
                printf("[reaper()→ Proceso hijo %d en foreground (%s) finalizado por señal %d]\n", jobs_list[i].pid, jobs_list[i].command_line, WTERMSIG(status));
            } else {
                // Informamos de lo ocurrido
                printf("[reaper()→ Proceso hijo %d en foreground (%s) finalizado con exit code %d]\n", jobs_list[i].pid, jobs_list[i].command_line, WEXITSTATUS(status));
            }  
       }
    }
    jobs_list_remove(i);
    printf("\r");
}





/******************************************************************************
*   NOMBRE:        CTRLC();
*   ARGUMENTOS:    INT SIGNUM
*   DEVUELVE:      VOID
*   DESCRIPCIÓN:   ENVIA LA SEÑAL SIGTERM AL PROCESO FOREGROUND PARA FINALIZAR 
*                  SU EJECUCIÓN, SI SE TRATA DEL MINISHELL NO SE LE ENVIARÁ
*                  DICHA SEÑAL.
 *****************************************************************************/
void ctrlc(int signum){
    // Volvemos a asociar la señal para que no se restaure por defecto
    signal(SIGINT, ctrlc);
    printf("\n[ctrlc()→ Soy el proceso con PID %d (%s), el proceso en foreground es %d (%s)]\n", getpid(), SHELL_COMMAND_LINE, jobs_list[0].pid, jobs_list[0].command_line);
    if(jobs_list[0].pid > 0){   // Si hay proceso en foreground
        // Lo detenemos, siempre y cuando no sea el propio shell
        if(strcmp(jobs_list[0].command_line, SHELL_COMMAND_LINE) != 0){
            // Asociamos la señal SIGTERM al proceso en foreground
            int error = kill(jobs_list[0].pid, SIGTERM);
            if(error == -1){ // error contiene -1 si hubo error
                fprintf(stderr, "Error: %s \n", strerror(errno));
            }
            printf("[ctrlc()→ Señal %d enviada a %d (%s) por %d (%s)]\n", SIGTERM, jobs_list[0].pid, jobs_list[0].command_line, getpid(), SHELL_COMMAND_LINE);
        } else {
            printf("[ctrlc()→ Señal %d no enviada debido a que el proceso en foreground es el shell]\n", SIGTERM);
        }
    } else {    // Si no, no hacemos nada
        printf("[ctrlc()→ Señal %d no enviada por %d debido a que no hay proceso en foreground]\n",SIGTERM, getpid());
    }
}


/******************************************************************************
*   NOMBRE:        CTRLZ();
*   ARGUMENTOS:    INT SIGNUM
*   DEVUELVE:      VOID
*   DESCRIPCIÓN:   DETIENE UN PROCESO EN FOREGROUND.
 *****************************************************************************/
void ctrlz(int signum){    
    // Asociamos a la señal SIGINT la función ctrlc para evitar incompatibilidad
    signal(SIGTSTP, ctrlz);
    // Asociamos a la señal SIGCHLD lo estandar, para no llamar al reaper al detener el proceso.
    //signal(SIGCHLD, SIG_DFL);
    if(jobs_list[0].pid != 0){
        if(strcmp(jobs_list[0].command_line, SHELL_COMMAND_LINE) != 0){        
            kill(jobs_list[0].pid, SIGTSTP);
            jobs_list[0].status = 'D';
            // Añadimos los datos del proceso detenido a job_list
            jobs_list_add(jobs_list[0].pid, jobs_list[0].status, jobs_list[0].command_line);
            printf("\n[ctrlz()→ Soy el proceso con PID %d, el proceso en foreground es %d (%s)]\n", getpid(),jobs_list[0].pid, jobs_list[0].command_line);                        
            printf("[ctrlz()→ Señal 20 (SIGTSTP) enviada a %d (%s) por %d (%s)]\n", jobs_list[0].pid, jobs_list[0].command_line, getpid(),SHELL_COMMAND_LINE);
            // Imprimimos en pantalla la información del proceso detenido.
            printf("\n[%d]\t%d\t%c\t%s \n", n_pids, jobs_list[n_pids].pid, jobs_list[n_pids].status, jobs_list[n_pids].command_line);
            // Reseteamos los valores del job_list[0]
            jobs_list[0].pid = 0;
            strcpy(jobs_list[0].command_line, "");
            jobs_list[0].status = 'F';
        }
        else {
            printf("\n[ctrlz()→ Soy el proceso con PID %d, el proceso en foreground es %d (%s)]\n", getpid(),jobs_list[0].pid, jobs_list[0].command_line);                        
            printf("[ctrlz()→ Señal 20 (SIGTSTP) no enviada a %d (%s) por %d (%s)]\n", jobs_list[0].pid, jobs_list[0].command_line, getpid(),SHELL_COMMAND_LINE);
        }
    }
    else {
        printf("\n");
    }
}

/******************************************************************************
*   NOMBRE:       IMPRIMIR_PROMPT();
*   ARGUMENTOS:   NINGUNO.
*   DEVUELVE:     NADA.
*   DESCRIPCIÃN:  LA FUNCIÓN IMPRIME UN ARRAY DE CHAR:
*                   - nombre_usuario-->directorio_actual$
 *****************************************************************************/
void imprimir_prompt(){
    // Reservamos espacio en memoria para el prompt.
    char *prompt = malloc(1000);
    // Si hay algÃºn error en malloc()
    if(prompt == NULL){
        fprintf(stderr, "Error: %s \n", strerror(errno));
    }
    // AÃ±adimos color azul.
    strcpy(prompt, COLOR_BLUE);
    // AÃ±adimos el nombre de usuario.
    strcat(prompt, getenv("USER"));
    // AÃ±adimos color amarillo.
    strcat(prompt, COLOR_YELLOW);
    // AÃ±adimos String separador.
    strcat(prompt, SEPARATOR);
    // AÃ±adimos color rojo.
    strcat(prompt, COLOR_RED);
    // AÃ±adimos directorio actual.
    strcat(prompt, getenv("PWD"));
    // AÃ±adimos el caracter prompt.
    strcat(prompt, PROMPT);
    // AÃ±adimos el color blanco.
    strcat(prompt, COLOR_WHITE);
    // AÃ±adimos un espacio en blanco.
    strcat(prompt, " ");
    // imprimimos el prompt.
    printf("%s", prompt);
    // Liberamos espacio de memoria.
    free(prompt);
}

/******************************************************************************
*   NOMBRE:       READ_LINE();
*   ARGUMENTOS:   PUNTERO DONDE SE GUARDA LA LÍNEA
*   DEVUELVE:     PUNTERO DONDE SE HA GUARDADO LA LINEA
*   DESCRIPCIÓN:  LEE COMANDOS INTRODUCIDOR EN STDIN
 *****************************************************************************/
char *read_line(char *line){  
    // Imprimimos el prompt.
    imprimir_prompt();
    // Limpiamos el buffer de salida.
    fflush(stdout);
    // Alojamos en line los caracteres introducidos por teclado.
    char * ptr = fgets(line, COMMAND_LINE_SIZE, stdin);
    if(!ptr){
        if(feof(stdin)){
            printf("\n");
            exit(EXIT_SUCCESS);
        }
        else{
           ptr = line; // si no al pulsar inicialmente CTRL+C sale fuera del shell
           ptr[0] = 0; // Si se omite esta línea aparece error ejecución ": no se encontró la orden"*/
        }

    }
    // Eliminamos el retorno de carro de line
    strtok(line, "\n");
    // Limpiamos el buffer de entrada
    fflush(stdin);
    // Devolvemos el puntero.
    return ptr;

}



/******************************************************************************
*   NOMBRE:       EXECUTE_LINE();
*   ARGUMENTOS:
*   DEVUELVE:     0 - Todo ha ido bien.
*                -1 - Ha habido algÃºn error.
*   DESCRIPCIÓN:  EJECUTA UNA LÍNEA DE COMANDOS.
 *****************************************************************************/
int execute_line(char *line){
    // Creamos doble puntero
    char **args = malloc(ARGS_SIZE * sizeof(char *));
    // Si args es NULL ha habido un problema con malloc()
    if(args == NULL){
        fprintf(stderr, "Error: %s \n", strerror(errno));
        return -1;
    }
    // Hacemos una copia de la linea para guardarla en la pila de procesos.
    char command_line[COMMAND_LINE_SIZE];
    strcpy(command_line, line); 
    strtok(command_line, "\n");
    // Guardamos en numTokens el nÃºmero de tokens de line
    int numTokens = parse_args(args, line);
    // Comprobamos si se trata de un999 comando interno y si lo es se ejecuta.
    int internalCommand = check_internal(args);
    // Si se trata de un comando externo
    if(internalCommand == 0){
        // Creamos un proceso hijo.
        int childPID = fork();
        int isBackground = is_background(command_line);
        if(childPID > 0){
            if(isBackground){
                jobs_list_add(childPID, 'E', command_line);
                // Imprimimos el ID del padre.
                printf("[execute_line()→ PID padre: %d]\n", getppid());
                // Imprimimos el ID del hijo.
                printf("[execute_line()→ PID hijo: %d]\n", getpid());
                printf("[%d]\t%d\tE\t%s\n", jobs_list_find(childPID), childPID, command_line);
            } else {
                // Añadimos el proceso hijo al array de procesos
                jobs_list[0].pid = childPID;
                jobs_list[0].status = 'E';
                strcpy(jobs_list[0].command_line, command_line);
            }
        }    
        // Si el que está en ejecución es el hijo.
        if(childPID == 0){
            if(isBackground){
                // EL proceso background ignora Control+Z
                signal(SIGTSTP, SIG_IGN);
                // Eliminamos el '&'
                if(args[numTokens-1][0] == 38){
                    // Eliminamos el último token.
                    args[numTokens-1] = NULL;
                } else {
                    // Eliminamos el último caracter del último token
                    args[numTokens-1][strlen(args[numTokens-1])-1] = '\0';
                }
            } else {
                // El proceso foreground no ignora Control+Z
                signal(SIGTSTP, SIG_DFL);
                // Imprimimos el ID del padre.
                printf("[execute_line()→ PID padre: %d]\n", getppid());
                // Imprimimos el ID del hijo.
                printf("[execute_line()→ PID hijo: %d]\n", getpid());
            }
            // El proceso hijo ignorará la señal de interrupción
            signal(SIGINT, SIG_IGN);
            // Y hará la función por defecto de la señal SIGCHLD
            signal(SIGCHLD, SIG_DFL);
            // Ejecutamos el comando con los argumentos.
            if(execvp(args[0], args) == -1){
                // Si el comando no existe.
                fprintf(stderr, "%s: no se encontró la orden \n", *args);
                exit(EXIT_FAILURE);
            }
            exit(EXIT_SUCCESS);
        } // Si el que está ejecución es el proceso hijo.
        else {
            while(jobs_list[0].pid != 0 ) {
                pause();
            }
        }

    }
    // Liberamos memoria del puntero.
    free(args);
    // Devolvemos 0.
    return 0;
}



/******************************************************************************
*   NOMBRE:       IS_BACKGROUND();
*   ARGUMENTOS:   PUNTERO CHAR 'LINE'.
*   DEVUELVE:     1 - EL PROCESO ES BACKGROUND.
*                 0 - EL PROCESO ES FOREGROUND.
*   DESCRIPCIÓN:  DETERMINA SI UN PROGRAMA SERÁ EJECUTADO EN BACKGROUND O 
*                 FOREGROUND
 *****************************************************************************/
int is_background(char *line){
    // Si el comando acaba en '&' (ASCII = 38)
    if(line[strlen(line)-1] == 38){
        // Devolvemos uno: es brackground.
        return 1;
    }
    // Si llegamos aquí es foreground.
    return 0;
}

/******************************************************************************
*   NOMBRE:       JOBS_LIST_FIND();
*   ARGUMENTOS:   PID_T PID.
*   DEVUELVE:     -1 - EL PROCESO NO HA SIDO ENCONTRADO
*                 I - INDICE DEL PROCESO.
*   DESCRIPCIÓN:  DEVUELVE EL INDICE DEL PROCESO EN LA PILA DE PROCESOS.
 *****************************************************************************/
int jobs_list_find(pid_t pid){
    for(int i = 0; i <= n_pids; i++){
        if(pid == jobs_list[i].pid){
            return i;
        }
    }
    return -1;
}


/******************************************************************************
*   NOMBRE:       JOBS_LIS_REMOVE();
*   ARGUMENTOS:   INT POSICIÓN
*   DEVUELVE:     
*   DESCRIPCIÓN:  ELIMINA UN PROCESO DE LA PILA DE PROCESOS.
 *****************************************************************************/
int jobs_list_remove(int pos){
    if(pos == 0){
        jobs_list[0].pid = 0;
        strcpy(jobs_list[0].command_line, "");
        jobs_list[0].status = 'F';
        return 0;
    }
    if(pos != n_pids){
        // Indicamos que no hay proceso en primer plano.
        jobs_list[pos].pid = jobs_list[n_pids].pid;
        // Indicamos que el proceso que se está ejecutando es el shell.
        strcpy(jobs_list[pos].command_line, jobs_list[n_pids].command_line);
        // Proceso finalizado.
        jobs_list[pos].status = jobs_list[n_pids].status;
    }
    n_pids--;
    return 0;
}

/******************************************************************************
*   NOMBRE:       JOB_LIST_ADD();
*   ARGUMENTOS:   PID_T PID, CHAR ESTADO, CHAR * COMMAND_LINE.
*   DEVUELVE:     0 - NO SE PUEDE ADMITIR PROCESO.
*                 1 - PROCESO AÑADIDO.
*   DESCRIPCIÓN:  AÑADE UN PROCESO A LA PILA DE PROCESOS.
 *****************************************************************************/
int jobs_list_add(pid_t pid, char status, char *command_line){
    if(n_pids >= N_JOBS){
        return 0;
    }
    n_pids++;
    jobs_list[n_pids].pid = pid;
    strcpy(jobs_list[n_pids].command_line, command_line);
    jobs_list[n_pids].status = status;
    return 1;
}

/******************************************************************************
*   NOMBRE:       PARSE_ARGS();
*   ARGUMENTOS:   DOBLE PUNTERO ARGS, PUNTERO LINE
*   DEVUELVE:     INT NÚMERO DE TOKENS
*   DESCRIPCIÓN: TOKENIZA LINE Y GUARDA LOS TOKENS EN ARGS.
 *****************************************************************************/
int parse_args(char **args, char *line){
    // Contador de tokens.
    int tokenCounter = 0;
    // Cogemos el primer token y lo guardamos.
    *args = strtok(line, TOKEN_KEYS);
    // Informamos por consola de lo ocurrido.
    //printf("[parse_args()â token %d: %s]\n",tokenCounter, *args);
    // Mientras haya tokens.
    while(*args != NULL){
        // Si el Token es un comentario.
        if(*args[0] == COMMENT_WILDCARD){
            // Nos aseguramos lo que viene detrÃ¡s no serÃ¡ tratado.
            *args = NULL;
            // Imprimimos mensaje de informaciÃ³n.
            //printf("[parse_args()-> token %d corregido: %s]\n", tokenCounter, *args);
        }
        // Si el token no es un comentario.
        else {
            // Avanzamos en el array.
            args++;
            // Actualizamos el contador de tokens.
            tokenCounter++;
            // Recogemos el siguiente token.
            *args = strtok(NULL, TOKEN_KEYS);
            // Imprimimos en pantalla lo ocurrido.
            //printf("[parse_args()â token %d: %s]\n",tokenCounter, *args);
        }
    }
    return tokenCounter;
}

/******************************************************************************
*   NOMBRE:       ChECK_INTERNAL();
*   ARGUMENTOS:   DOBLE PUNTERO ARGS
*   DEVUELVE:     1 - Comando interno
*                 0 - Comando externo
*                -1 - Comando nulo (NULL)
*   DESCRIPCIÓN: COMPRUEBA SI SE TRATA DE UN COANDO INTERNO, EN TAL CASO LO
*                EJECUTA. SI ES UN COMANDO EXTERNO DEVUELVE 1.
 *****************************************************************************/
int check_internal(char **args){
    // Si no hay ningÃºn token.
    if(*args == NULL) return -1;
    // Si se ha introducido "exit".
    if(strcmp("exit", *args) == 0)      exit(0);
    // Si se ha introducido "cd"
    if(strcmp("cd", *args) == 0)        return internal_cd(args);
    // Si se ha introducido "export"
    if(strcmp("export", *args) == 0)    return internal_export(args);
    // Si se ha introducido "source"
    if(strcmp("source", *args) == 0)    return internal_source(args);
    // Si se ha introducido "jobs"
    if(strcmp("jobs", *args) == 0)      return internal_jobs(args);
    // Si hemos llegado hasta aquÃ­ significa que es un comando externo.
    return 0;
}

/******************************************************************************
*   NOMBRE:       INTERNAL_CD();
*   ARGUMENTOS:   DOBLE ÙNTERO ARGS.
*   DEVUELVE:     ENTERO INFORMANDO DE CÓMO HA ACABADO.
*   DESCRIPCIÓN:  CAMBIA DE DIRECTORIO.
 *****************************************************************************/
int internal_cd(char **args){
    // Directorio Actual
    char actualDir[COMMAND_LINE_SIZE];
    // Avanzamos al siguiente token
    args++;
    // Declaramos un puntero tipo char para alojar el directorio al que iremos.
    char *nextDir = malloc(COMMAND_LINE_SIZE);
    // Si ha habido algun error en el malloc()
    if(nextDir == NULL){
        fprintf(stderr, "Error: %s \n", strerror(errno));
        return -1;
    }
    // Si no hay argumento
    if(*args == NULL){
        strcpy(nextDir, "/home/");
        strcat(nextDir, getenv("USER"));
    } else { // Si hay argumento
        // Comprobammos si la sintaxis es correcta
        if(check_cd(args)!= 1){
            // Imprimimos informaciÃ³n del error
            fprintf(stderr, "Error de sintaxis. Demasiados argumentos\n");
            // Devolvemos -1 para saber que es un error.
            return -1;
        }
        // Concatenamos los tokens
        concatenateTokens(args, nextDir);
        // Reemplazamos los caracteres '\' si los hubiera
        replaceChar(nextDir, 92);
        // Limpiamos caracteres "  y ' si los hubiera
        cleanString(nextDir);
    }
    // Si se produce un error al cambiar de directorio
    if(chdir(nextDir) != -1){
        // Cambiamos el valor de la variable de entorno PWD
        if(getcwd(actualDir, sizeof(actualDir)) == NULL){
            fprintf(stderr, "%s\n", strerror(errno));
        }
        else{
            setenv("PWD", actualDir,1);
        }
        // Imprimimos la informaciÃ³n.
        //printf("[internal_cd()-> %s]\n", actualDir);
    } else { // Si se ha producido un error
        // Imprimimos el error.
        fprintf(stderr, "chdir: %s \n", strerror(errno));
        // Devolvemos -1 porque ha habido un error.
        return -1;
    }
    free(nextDir);
    return 1;
}

/******************************************************************************
*   NOMBRE:       CHECK_CD();
*   ARGUMENTOS:   DOBLE PUNTERO ARGS.
*   DEVUELVE:     DEVUELVE ENTERO DEPENDIENDO DE SI ESTÁ BIEN O NO EL ARGS
*   DESCRIPCIÓN:  COMPRUEBA SI LA SINTAXIS ES CORRECTA PARA INTERNAL_CD()
 *****************************************************************************/
int check_cd(char **args){
    // Si hay directorio con espacios, el Ãºltimo elemento del token0 serÃ¡ "\"
    if(args[0][strlen(args[0])-1] == 92){
        // Recorremos los tokens
        for(int i = 1; args[i] != NULL; i++){
            // Si encontramos un token que no tenga "\" final es que hemos acabado
            if(args[i][strlen(args[i])-1] != 92){
                // Si el siguiente token no es null hay un error de sintaxis.
                if(args[i+1] != NULL) return -1;
            }
        }
    }
    // Si hay directorio con espacios, el token0 tendrÃ¡ el caracter "
    if(strchr(args[0], 34) != NULL){
        for(int i = 1; args[i] != NULL; i++){
            if(strchr(args[i], 34) != NULL){
                // Si el siguiente token no es null hay un error de sintaxis.
                if(args[i+1] != NULL) return -1;
            }
        }
    }
   // Si hay directorio con espacios, el token0 tendrÃ¡ el caracter '
    if(strchr(args[0], 39) != NULL){
        for(int i = 1; args[i] != NULL; i++){
            if(strchr(args[i], 39) != NULL){
                // Si el siguiente token no es null hay un error de sintaxis.
                if(args[i+1] != NULL) return -1;
            }
        }
    }
    return 1;
}
/******************************************************************************
*   NOMBRE:       CONCATENATETOKENS();
*   ARGUMENTOS:   DOBLE PUNTERO ARGS, PUNTERO NEXTDIR
*   DEVUELVE:     VOID
*   DESCRIPCIÃN: CONCATENA LOS ARGUMENTOS EN NEXTDIR
 *****************************************************************************/
void concatenateTokens(char **args, char *nextDir){
    // Variable 'booleana' que controla si se encuentr o no un caracter especial
    int boolean = 0;
    // Mientras haya tokens
    for(int i=0;args[i] != NULL;i++){
        // Concatenamos el token
        strcat(nextDir, args[i]);
        // Si encontramos un caracter ' o "
        if(strchr(args[i], 39) || strchr(args[i], 34)){
            // Si habiamos encontrado uno
            if(boolean == 1){
                // Ahora encontramos la pareja.
                boolean = 0;
            } else{ // Si no haÃ­amos encontrado ninguno
                // Ahora hemos encontrado el primero
                boolean = 1;
            }
        }
        // Si hay un caracter especial 
        if(boolean == 1){
            // Concatenamos un espacio porque "Directorio con espacios"
            strcat(nextDir, " ");
        }
    }

}

/******************************************************************************
*   NOMBRE:       REPLACECHAR();
*   ARGUMENTOS:   PUNTERO CHAR STRING, INT VALOR ASCII
*   DEVUELVE:     VOID
*   DESCRIPCIÓN:  REEMPLAZA UN CARACTER POR EL VALOR DEL CARACTER PARÁMETRO.
 *****************************************************************************/
void replaceChar(char *string, int replacedChar){
    // Mientras no lleguemos al final de string
    for(int i = 0;string[i] != '\0'; i++){
        // Si el caracter es igual al caracter que queremos reemplazar
        if(string[i] == replacedChar){
            // Reemplazamos ese elemento por un espacio
            string[i] = ' ';
        }
    }
}

/******************************************************************************
*   NOMBRE:       CLEANSTRING();
*   ARGUMENTOS:   PUNTERO STRING
*   DEVUELVE:     VOID
*   DESCRIPCIÓN:  ELIMINA CIERTOS CARACTERES COMO SON " Y '.
 *****************************************************************************/
void cleanString(char *string){
    // Variables Ã­ndices de control
    int j = 0;
    int k = 0;
    // Recorremos el string
    for(int i = 0;string[i] != '\0'; i++){
        // Si encontramos un caracter ' o ""
        if(string[i] == 34 || string[i] == 39){
            j = i;
            k = i + 1;
            // Recorremos el resto para actualizar el array
            while(string[k] != '\0'){
                string[j] = string[k];
                j++;
                k++;
            }
            // Final de string
            string[j] = '\0';
        }
   }
}



/******************************************************************************
*   NOMBRE:       INTERNAL_EXPORT();
*   ARGUMENTOS:   DOBE PUNTERO ARGS
*   DEVUELVE:     INT INFORMANDO DEL ESTADO
*   DESCRIPCIÓN:  CAMBIA VALORES DE VARIABLES DE ENTORNO.
 *****************************************************************************/
int internal_export(char **args){
    args++;
    // Si se introduce sólo 'export' salta este error.
    if(*args == NULL){
        fprintf(stderr,"Error de sintaxis. Uso: export Nombre=Valor \n");
        return -1;
    }
    // Comprobamos que solo hay 2 tokens, si no, error
    args++;
    // Si hay un tercer token, error
    if(*args != NULL){
        fprintf(stderr,"Error de sintaxis. Uso: export Nombre=Valor \n");
        return -1;
    }
    // Volvemos a colocar la instrucciÃ³n en el puntero
    args--;
    // Creamos dos punteros para trocear la instrucciÃ³n
    char *nombre = malloc(sizeof(COMMAND_LINE_SIZE)); // antiguo nombre de la variable
    char *valor = malloc(sizeof(COMMAND_LINE_SIZE));  // nuevo nombre de la variable
    // Guardamos lo anterior al "=" en 'nombre'
    int i;
    for(i = 0; args[0][i] != '=' && args[0][i] != '\0'; i++){
        nombre[i] = args[0][i];
    }
    nombre[i] = '\0';
    // Si no hay "=" en el medio, es un error de sintaxis
    if(args[0][i] != '='){
        //printf("[internal_export()â nombre: %s]\n", nombre);
        valor = NULL;
        //printf("[internal_export()â valor: %s]\n", valor);
        fprintf(stderr,"Error de sintaxis. Uso: export Nombre=Valor \n");
        return -1;
    }
    i++;
    // Guardamos el string posterior al "=" en 'valor'
    int a;
    for(a = 0; args[0][i] != '\0'; a++){
        valor[a] = args[0][i];
        i++;
    }
    valor[a] = '\0';
    // Imprimimos por pantalla los cambios realizados
    //printf("[internal_export() -> nombre: %s]\n", nombre);
    //printf("[internal_export() -> valor: %s]\n", valor);
    //printf("[internal_export() -> antiguo valor para %s: %s]\n", nombre, getenv(nombre));
    // Cambiamos el valor de la variable
    setenv(nombre, valor, 1);
    //printf("[internal_export() -> nuevo valor para %s: %s]\n", nombre, getenv(nombre));
    free(nombre);
    free(valor);
    return 1;
}

/******************************************************************************
*   NOMBRE:       INTERNAL_SOURCE();
*   ARGUMENTOS:   Nombre del fichero
*   DEVUELVE:     1
*   DESCRIPCIÃN:  Ejecuta los comandos de las lineas del fichero indicado.
 *****************************************************************************/
int internal_source(char **args){
    // Comprobamos primero que la sintaxis es correcta
    if(args[2] == NULL){
        if(args[1] != NULL){
            // Abrimos el fichero
            FILE * file = fopen(args[1], "r");
            // Creamos una array donde se alojarÃ¡n los datos
            char linea[COMMAND_LINE_SIZE];
            if(file){
                // Comenzamos la lectura del fichero
                char *pointer = fgets(linea, COMMAND_LINE_SIZE, (FILE*) file);
                // DespuÃ©s de cada lectura, realizamos un fflush
                fflush(file);
                while(pointer != NULL){
                    //printf("[internal_source()â LINE: %s ]\n", pointer);
                    // Ejecutamos las lineas que leemos del fichero
                    execute_line(pointer);
                    pointer = fgets(linea, COMMAND_LINE_SIZE, (FILE*) file);
                    fflush(file);
                }
                fclose(file);
            } else {
                // Si no existe el fichero, error
                fprintf(stderr, "fopen: %s\n", strerror(errno));
            }
        } else{
            // Si no se escribe el nombre de fichero como parÃ¡metro, error
            fprintf(stderr, "Error de sintaxis. Uso: source <nombre_fichero>\n");
        }
    } else {
        // Si hay más de dos tokens, error
        fprintf(stderr, "Error de sintaxis. Uso: source <nombre_fichero>\n");
    }
    return 1;  
}

/******************************************************************************
*   NOMBRE:       INTERNAL_JOBS();
*   ARGUMENTOS:   DOBLE PUNTERO ARGS
*   DEVUELVE:     INT DEPENDIENDO DEL ESTADO QUE ACABÓ.
*   DESCRIPCIÓN:  MUESTRA LOS TRABAJOS EN SEGUNDO PLANO EN EJECUCIÓN O DETENIDOS.
 *****************************************************************************/
int internal_jobs(char **args){
    for(int i = 1; i <= n_pids; i++ ){
        if(jobs_list[i].status == 'E' || jobs_list[i].status == 'D'){
        printf("[%d]\t%d\t%c\t%s\n", i, jobs_list[i].pid, jobs_list[i].status, jobs_list[i].command_line);
        }
    }
    return 1;
}

